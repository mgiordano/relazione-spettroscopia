# il terminale sarà da cambiare
set term epslatex size 9,4

# settaggi generali
set xtics autofreq 20
set format '\num{%g}'
set logscale y # asse y logaritmico
set xlabel "lunghezza d'onda (\\si{\\nano\\metre})" # etichetta asse x
set ylabel "potenza (\\si{\\nano\\watt})" offset 5,0 # etichetta asse y
unset key # tolgo legenda

# spettro del rubidio
set output "rubidio.tex"
set xrange [400:820]
set yrange [1:1e4]
# linee in corrispondenza delle righe attese per il rubidio
set for [i in "420.8705 543.153 572.445 629.833 727.999 740.817 780.023 \
    794.760"] arrow from i,1 to i,1e4 nohead linetype 0 linewidth 3
plot "./dati.dat" index 0:0 with points pointtype 7

# spettro dell'elio
unset arrow
set output "elio.tex"
set xrange [400:750]
set yrange [1:2e3]
# linee in corrispondenza delle righe attese per l'elio
set for [i in "413.2285 447.148 471.314 501.567 587.562 667.812 706.519 \
    728.135"] arrow from i,1 to i,2e3 nohead linetype 0 linewidth 3
plot "./dati.dat" index 1:1 with points pointtype 7
set output # chiudo il file di output
