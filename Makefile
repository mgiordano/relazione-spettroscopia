MAIN		= relazione4
MAIN_TEX	= $(MAIN).tex
MAIN_PDF	= $(MAIN).pdf
CLEAN_FILES	= *.aux *eps-converted-to.pdf *.fdb_latexmk *.log *~
DISTCLEAN_FILES	= *.eps relazione4.pdf elio.tex elio.pdf

.PHONY: pdf clean distclean

pdf: $(MAIN_PDF)

$(MAIN_PDF): $(MAIN_TEX) elio.tex rubidio.tex
	latexmk -pdf $(MAIN)

elio.tex: grafici.gnuplot dati.dat
	gnuplot grafici.gnuplot

clean:
	rm -f $(CLEAN_FILES)

distclean: clean
	rm -f $(DISTCLEAN_FILES)
